import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { NavController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { NgZone } from '@angular/core';
import 'rxjs/add/operator/map'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  mylaImage:string='assets/imgs/logo.png';
  mylaImageDefault:string='assets/imgs/logo.png';
  mylaImageSpeak:string='assets/imgs/logo_speak.png';
  mylaImageWorking:string='assets/imgs/logo_working.png';

  isListening: boolean = false;
  matches: Array<String>;
  serviceURL:string = 'http://frankkuhn.com:16003/myla';

  constructor(public http: Http, public navCtrl: NavController, public tts: TextToSpeech, public speech: SpeechRecognition, private zone: NgZone) {

  }

  async hasPermission():Promise<boolean> {
    try {
      const permission = await this.speech.hasPermission();
      return permission;
    } catch(e) {
      this.sayMessage('Fehler: Keine Berechtigungen.');
    }
  }

  async getPermission():Promise<void> {
    try {
      this.speech.requestPermission();
    } catch(e) {
      this.sayMessage('Fehler: Problem beim berechtigen.');
    }
  }

  ngOnInit() {
    this.sayMessage('Hallo. Ich bin Maila, dein persönlicher Assistent.');
  }

  sayMessage(message:string):void {
    this.tts.speak({
	    text: message,
	    locale: 'de-DE',
	    rate: 1.0
    });
  }

  listen() {
    this.mylaImage = this.mylaImageDefault;
    if (this.isListening) {
      this.speech.stopListening();
      this.toggleListenMode();
      this.sayMessage('Okay');
      return;
    }

    this.mylaImage = this.mylaImageSpeak;
    
    this.toggleListenMode();
    let self = this;

    this.sayMessage('Leg los.');

    let options = {
      language: 'de-DE',
      matches: 1,
      showPopup: false,
      showPartial: false
    }

    this.speech.startListening(options).subscribe(
      matches => {
        self.zone.run(() => {
          if (matches && matches.length > 0) {
            // TODO: Language from profile
            var recordedData = {'message': matches[0], 'language': 'DE'};
            self.process(recordedData);
          } else {
            self.sayMessage('Ich habe dich nicht verstanden.');
          }
        })
      },
      error => this.sayMessage('Fehler: ' + error)
    );
  }

  toggleListenMode():void {
    this.isListening = this.isListening ? false : true;
  }

  process(data: any = {}) {
    var self = this;

    this.mylaImage = this.mylaImageWorking;

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.http.post(this.serviceURL, data, options)
      .map(res => res.json())
      .subscribe(response =>
        {
            // Processing response
            var action = response.action;
            var speak = response.speak;

            self.sayMessage(speak);

            switch(action) {
              case 'note':
                // TODO: execute task
                break;
              case 'weather':
                // TODO: execute task
                break;
              default:
                break;
            }

            this.mylaImage = this.mylaImageDefault;
      });
  }
}